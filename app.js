const express = require('express');
const cors = require('cors');
const helmet = require('helmet');
const compression = require('compression');
const xss = require('xss-clean');
const ytdl = require('ytdl-core');
const nodemailer = require('nodemailer');
const AWS = require('aws-sdk');

// calling express function and store into a variable
const app = express();

// i) Implemented cors
app.use(cors());
app.options('*', cors());
app.use(express.json({ limit: '10kb' }));

// ii) Set security HTTP headers
app.use(helmet());

// iii) Data sanitization against XSS (Cross-site scripting)
app.use(xss());

// iv) Compressing
app.use(compression());

// Email
const transport = nodemailer.createTransport({
  host: 'sandbox.smtp.mailtrap.io',
  port: 2525,
  auth: {
    user: '22c8a7b9154634',
    pass: 'eef9c6f91851cb',
  },
});

const credentials = new AWS.SharedIniFileCredentials({ AWS_SECRET_ACCESS_KEY: `44hUGtsHgVQQqos+4i+omqBClqgwkDU4/y6cp7/n`, AWS_ACCESS_KEY: `AKIAQJOTQHQFM2VEXQLB` });
AWS.config.credentials = credentials;

console.log('Credentials', credentials);

// Specify the region.
AWS.config.update({ region: 'me-south-1',apiVersion: 'latest', accessKeyId: `AKIAQJOTQHQFM2VEXQLB`, secretAccessKey: `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` });

const pinpointEmail = new AWS.PinpointEmail();

// Route handler
app.get('/api/v1/yt/:id', async (req, res) => {
  const { id } = req.params;

  if (!id) {
    return res.status(400).json({
      success: false,
      message: 'Please provide a YouTube video ID',
    });
  }

  const result = ytdl.validateID(id);

  if (!result) {
    return res.status(404).json({
      success: false,
      message: `Sorry! Can not find YouTube video with this ID: ${id}`,
    });
  }

  try {
    const info = await ytdl.getInfo(id);
    const audioFormats = ytdl.filterFormats(info.formats, 'audioonly');

    if (audioFormats && audioFormats.length) {
      const response = await fetch(audioFormats[0].url);
      const arrayBuffer = await response.arrayBuffer();
      const buffer = Buffer.from(arrayBuffer);
      res.setHeader('Content-Type', 'audio/mpeg');
      res.send(buffer);
    } else {
      return res.status(502).json({
        success: false,
        message: 'Bad Gateway',
        data: null,
      });
    }
  } catch (error) {
    return res.status(500).json({
      success: false,
      message: error?.message || 'Something went wrong',
    });
  }
});

app.post('/send', async (req, res) => {
  const { email_address, subject, first_name, last_name, phone_number, message, city } = req.body;

  const mailOptions = {
    from: email_address,
    to: '800supervision@uma.com.sa',
    subject,
    text: `
    Subject - ${subject}
    From - ${first_name} ${last_name}
    Phone Number - ${phone_number}
    Email - ${email_address}
    City - ${city}
    Message - ${message}`,
    html: `
    <!doctype html>
<html lang="en-US">

<head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
    <title>New Account Email Template</title>
    <meta name="description" content="New Account Email Template.">
    <style type="text/css">
        a:hover {
            text-decoration: underline !important;
        }
    </style>
</head>

<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; background-color: #f2f3f8;" leftmargin="0">
    <!-- 100% body table -->
    <table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8"
        style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: 'Open Sans', sans-serif;">
        <tr>
            <td>
                <table style="background-color: #f2f3f8; max-width:670px; margin:0 auto;" width="100%" border="0"
                    align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="height:20px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                            <a href="https://www.mycar-india.com/" title="logo" target="_blank">
                                <img width="200" src="https://www.mycar-india.com/assets/Images/company-logo-en.svg" title="logo" alt="logo">
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:20px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0"
                                style="max-width:670px; background:#fff; border-radius:3px; text-align:center;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
                                <tr>
                                    <td style="height:40px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="padding:0 35px;">
                                        <h1
                                            style="color:#1e1e2d; font-weight:500; margin:0;font-size:32px;font-family:'Rubik',sans-serif;">
                                            ${subject}
                                        </h1>
                                        <p style="font-size:15px; color:#455056; margin:8px 0 0; line-height:24px;">
                                            ${message}</strong>.</p>
                                        <span
                                                style="display:inline-block; vertical-align:middle; margin:29px 0 26px; border-bottom:1px solid #cecece; width:100px;"></span>
                                        <p
                                            style="color:#455056; font-size:18px;line-height:20px; margin:0; font-weight: 500;">
                                            <strong
                                                    style="display: block;font-size: 13px; margin: 0 0 4px; color:rgba(0,0,0,.64); font-weight:normal;">Sender</strong>${first_name}
                                            ${last_name}
                                            <strong
                                                    style="display: block; font-size: 13px; margin: 24px 0 4px 0; font-weight:normal; color:rgba(0,0,0,.64);">Contact</strong>
                                        <div style="display: flex; flex-direction: column">
                                            <div style="display: flex; align-items: center; justify-content: center">
                                                <p style="
                                                  color: #455056;
                                                  font-size: 18px;"
                                                  >
                                                  Phone : </p> <a style="text-decoration: none" href="tel:${phone_number}">${phone_number}</a>
                                            </div>
                                            <div style="display: flex; align-items: center; justify-content: center">
                                                <p style="
                                                color: #455056;
                                                font-size: 18px;">Email : </p> <a style="text-decoration: none" href="mailto:${email_address}">${email_address}</a>
                                            </div>
                                            <div style="display: flex; align-items: center; justify-content: center">
                                                <p style="
                                                color: #455056;
                                                font-size: 18px;">City : ${city}</p> 
                                            </div>
                                        </div>
                                        </p>

                                    </td>
                                </tr>
                                <tr>
                                    <td style="height:40px;">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:20px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                            <p
                                style="font-size:14px; color:rgba(69, 80, 86, 0.7411764705882353); line-height:18px; margin:0 0 0;">
                                &copy; <strong>https://www.mycar-india.com</strong> </p>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:80px;">&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>

</html>`,
  };

  try {
    await transport.sendMail(mailOptions);

    return res.status(200).json({
      success: true,
      message: `email send to ${first_name} ${last_name}`,
    });
  } catch (error) {
    return res.status(500).json({
      success: false,
      message: error,
    });
  }
});

app.post('/order-mail', async (req, res) => {
  const {
    title,
    first_name,
    last_name,
    email,
    contact_number,
    nationality,
    address_1,
    address_2,
    city,
    payment_order_type,
    purchase_company,
    terms_condition,
    vehicle,
    vehicleImage,
    vehiclePrice,
    vinNumber,
    stockNumber,
  } = req.body;

  const mailOptions = {
    from: email,
    to: '800supervision@uma.com.sa',
    subject: 'Purchase Request',
    text: `
    Customer - ${title} ${first_name} ${last_name}
    Phone Number - ${contact_number}
    Email - ${email}
    Address - ${address_1}, ${address_2}, ${city}
    Nationality - ${nationality}
    Payment Type - ${payment_order_type === '1' ? 'Cash Reserve' : 'Finance'}
    Vehicle - ${vehicle},
    Vehicle Price - ${vehiclePrice}
    Vehicle Image - ${vehicleImage}
    `,
    html: `<html lang="en">

    <head>
      <meta charset="UTF-8" />
      <title>Purchase Request</title>
    
      <link rel="canonical" href="https://codepen.io/hermanshaho/pen/PopKNRQ" />
    
      <style>
        body {
          font-family: Helvetica, sans-serif;
          font-size: 13px;
        }
    
        .container {
          max-width: 680px;
          margin: 0 auto;
        }
      </style>
    </head>
    
    <body translate="no">
      <div class="container">
        <table width="100%">
          <tbody>
            <tr>
              <td>
                <div style="
                      color: #fff;
                      width: 150px;
                      height: 75px;
                      line-height: 75px;
                      text-align: center;
                      font-size: 11px;
                      display: flex;
                      align-items: center;
                    ">
                  <img src="https://www.mycar-india.com/assets/Images/company-logo-en.svg" alt="Compnay Logo"
                    width="100%" />
                </div>
              </td>
              <td width="100%" style="
                    background: #fee977;
                    border-left: 15px solid #fff;
                    padding-left: 30px;
                    font-size: 26px;
                    font-weight: bold;
                    letter-spacing: -1px;
                    text-align: center;
                  ">
                Purchase Request
              </td>
              <td></td>
            </tr>
          </tbody>
        </table>
        <h3>Contact details</h3>
        <table width="100%" style="border-collapse: collapse">
          <tbody>
            <tr>
              <td width="180px" style="
                    background: #eee;
                    text-transform: uppercase;
                    padding: 15px 5px 15px 15px;
                    font-size: 11px;
                  ">
                Customer
              </td>
              <td></td>
              <td style="border-top: 1px solid #eee; border-bottom: 1px solid #eee">
                ${title} ${first_name} ${last_name}
              </td>
              <td></td>
            </tr>
            <tr>
              <td style="
                    background: #eee;
                    text-transform: uppercase;
                    padding: 15px 5px 15px 15px;
                    font-size: 11px;
                  ">
                Phone Number
              </td>
              <td></td>
              <td style="border-top: 1px solid #eee; border-bottom: 1px solid #eee">
                ${contact_number}
              </td>
              <td></td>
            </tr>
            <tr>
              <td style="
                    background: #eee;
                    text-transform: uppercase;
                    padding: 15px 5px 15px 15px;
                    font-size: 11px;
                  ">
                Email
              </td>
              <td></td>
              <td style="border-top: 1px solid #eee; border-bottom: 1px solid #eee">
                ${email}
              </td>
              <td></td>
            </tr>
            <tr>
              <td style="
                    background: #eee;
                    text-transform: uppercase;
                    padding: 15px 5px 15px 15px;
                    font-size: 11px;
                  ">
                Address
              </td>
              <td></td>
              <td style="border-top: 1px solid #eee; border-bottom: 1px solid #eee">
                ${address_1}, ${address_2}, ${city}
              </td>
              <td></td>
            </tr>
            <tr>
              <td style="
                    background: #eee;
                    text-transform: uppercase;
                    padding: 15px 5px 15px 15px;
                    font-size: 11px;
                  ">
                Nationality
              </td>
              <td></td>
              <td style="border-top: 1px solid #eee; border-bottom: 1px solid #eee">
                ${nationality}
              </td>
              <td></td>
            </tr>
            <tr>
              <td style="
                    background: #eee;
                    text-transform: uppercase;
                    padding: 15px 5px 15px 15px;
                    font-size: 11px;
                  ">
                Payment Type
              </td>
              <td></td>
              <td style="border-top: 1px solid #eee; border-bottom: 1px solid #eee">
                ${payment_order_type === '1' ? 'Cash Reserve' : 'Finance'}
              </td>
              <td></td>
            </tr>
            <tr>
              <td style="
                    background: #eee;
                    text-transform: uppercase;
                    padding: 15px 5px 15px 15px;
                    font-size: 11px;
                  ">
                Vehicle
              </td>
              <td></td>
              <td style="border-top: 1px solid #eee; border-bottom: 1px solid #eee">
                ${vehicle}
              </td>
              <td></td>
            </tr>
            <tr>
              <td style="
                    background: #eee;
                    text-transform: uppercase;
                    padding: 15px 5px 15px 15px;
                    font-size: 11px;
                  ">
                Price
              </td>
              <td></td>
              <td style="border-top: 1px solid #eee; border-bottom: 1px solid #eee">
                ${vehiclePrice} SAR
              </td>
              <td></td>
            </tr>
            <tr>
            <td style="
                  background: #eee;
                  text-transform: uppercase;
                  padding: 15px 5px 15px 15px;
                  font-size: 11px;
                ">
              VIN Number
            </td>
            <td></td>
            <td style="border-top: 1px solid #eee; border-bottom: 1px solid #eee">
              ${vinNumber}
            </td>
            <td></td>
          </tr>
          <tr>
            <td style="
                  background: #eee;
                  text-transform: uppercase;
                  padding: 15px 5px 15px 15px;
                  font-size: 11px;
                ">
              Stock No
            </td>
            <td></td>
            <td style="border-top: 1px solid #eee; border-bottom: 1px solid #eee">
              ${stockNumber}
            </td>
            <td></td>
          </tr>
          </tbody>
        </table>
    
        <h3>${vehicle}</h3>
        <table width="100%" style="border-collapse: collapse; border-bottom: 1px solid #eee">
          <tbody>
            <tr>
              <img src="${vehicleImage}" alt="" style="width: 100%; height: 500px" />
            </tr>
          </tbody>
        </table>
      </div>
      <!-- container -->
    </body>
    
    </html>`,
  };

    // await transport.sendMail(mailOptions);
    const params = {
      FromEmailAddress: 'Vyrazu <ajaykrmahato@vyrazu.com>',
      Destination: {
        ToAddresses: ['sourav.mandal@vyrazu.com'],
      },
      Content: {
        Simple: {
          Body: {
            Html: {
              Data: `<html lang="en">

            <head>
              <meta charset="UTF-8" />
              <title>Purchase Request</title>
            
              <link rel="canonical" href="https://codepen.io/hermanshaho/pen/PopKNRQ" />
            
              <style>
                body {
                  font-family: Helvetica, sans-serif;
                  font-size: 13px;
                }
            
                .container {
                  max-width: 680px;
                  margin: 0 auto;
                }
              </style>
            </head>
            
            <body translate="no">
              <div class="container">
                <table width="100%">
                  <tbody>
                    <tr>
                      <td>
                        <div style="
                              color: #fff;
                              width: 150px;
                              height: 75px;
                              line-height: 75px;
                              text-align: center;
                              font-size: 11px;
                              display: flex;
                              align-items: center;
                            ">
                          <img src="https://www.mycar-india.com/assets/Images/company-logo-en.svg" alt="Compnay Logo"
                            width="100%" />
                        </div>
                      </td>
                      <td width="100%" style="
                            background: #fee977;
                            border-left: 15px solid #fff;
                            padding-left: 30px;
                            font-size: 26px;
                            font-weight: bold;
                            letter-spacing: -1px;
                            text-align: center;
                          ">
                        Purchase Request
                      </td>
                      <td></td>
                    </tr>
                  </tbody>
                </table>
                <h3>Contact details</h3>
                <table width="100%" style="border-collapse: collapse">
                  <tbody>
                    <tr>
                      <td width="180px" style="
                            background: #eee;
                            text-transform: uppercase;
                            padding: 15px 5px 15px 15px;
                            font-size: 11px;
                          ">
                        Customer
                      </td>
                      <td></td>
                      <td style="border-top: 1px solid #eee; border-bottom: 1px solid #eee">
                        ${title} ${first_name} ${last_name}
                      </td>
                      <td></td>
                    </tr>
                    <tr>
                      <td style="
                            background: #eee;
                            text-transform: uppercase;
                            padding: 15px 5px 15px 15px;
                            font-size: 11px;
                          ">
                        Phone Number
                      </td>
                      <td></td>
                      <td style="border-top: 1px solid #eee; border-bottom: 1px solid #eee">
                        ${contact_number}
                      </td>
                      <td></td>
                    </tr>
                    <tr>
                      <td style="
                            background: #eee;
                            text-transform: uppercase;
                            padding: 15px 5px 15px 15px;
                            font-size: 11px;
                          ">
                        Email
                      </td>
                      <td></td>
                      <td style="border-top: 1px solid #eee; border-bottom: 1px solid #eee">
                        ${email}
                      </td>
                      <td></td>
                    </tr>
                    <tr>
                      <td style="
                            background: #eee;
                            text-transform: uppercase;
                            padding: 15px 5px 15px 15px;
                            font-size: 11px;
                          ">
                        Address
                      </td>
                      <td></td>
                      <td style="border-top: 1px solid #eee; border-bottom: 1px solid #eee">
                        ${address_1}, ${address_2}, ${city}
                      </td>
                      <td></td>
                    </tr>
                    <tr>
                      <td style="
                            background: #eee;
                            text-transform: uppercase;
                            padding: 15px 5px 15px 15px;
                            font-size: 11px;
                          ">
                        Nationality
                      </td>
                      <td></td>
                      <td style="border-top: 1px solid #eee; border-bottom: 1px solid #eee">
                        ${nationality}
                      </td>
                      <td></td>
                    </tr>
                    <tr>
                      <td style="
                            background: #eee;
                            text-transform: uppercase;
                            padding: 15px 5px 15px 15px;
                            font-size: 11px;
                          ">
                        Payment Type
                      </td>
                      <td></td>
                      <td style="border-top: 1px solid #eee; border-bottom: 1px solid #eee">
                        ${payment_order_type === '1' ? 'Cash Reserve' : 'Finance'}
                      </td>
                      <td></td>
                    </tr>
                    <tr>
                      <td style="
                            background: #eee;
                            text-transform: uppercase;
                            padding: 15px 5px 15px 15px;
                            font-size: 11px;
                          ">
                        Vehicle
                      </td>
                      <td></td>
                      <td style="border-top: 1px solid #eee; border-bottom: 1px solid #eee">
                        ${vehicle}
                      </td>
                      <td></td>
                    </tr>
                    <tr>
                      <td style="
                            background: #eee;
                            text-transform: uppercase;
                            padding: 15px 5px 15px 15px;
                            font-size: 11px;
                          ">
                        Price
                      </td>
                      <td></td>
                      <td style="border-top: 1px solid #eee; border-bottom: 1px solid #eee">
                        ${vehiclePrice} SAR
                      </td>
                      <td></td>
                    </tr>
                    <tr>
                    <td style="
                          background: #eee;
                          text-transform: uppercase;
                          padding: 15px 5px 15px 15px;
                          font-size: 11px;
                        ">
                      VIN Number
                    </td>
                    <td></td>
                    <td style="border-top: 1px solid #eee; border-bottom: 1px solid #eee">
                      ${vinNumber}
                    </td>
                    <td></td>
                  </tr>
                  <tr>
                    <td style="
                          background: #eee;
                          text-transform: uppercase;
                          padding: 15px 5px 15px 15px;
                          font-size: 11px;
                        ">
                      Stock No
                    </td>
                    <td></td>
                    <td style="border-top: 1px solid #eee; border-bottom: 1px solid #eee">
                      ${stockNumber}
                    </td>
                    <td></td>
                  </tr>
                  </tbody>
                </table>
            
                <h3>${vehicle}</h3>
                <table width="100%" style="border-collapse: collapse; border-bottom: 1px solid #eee">
                  <tbody>
                    <tr>
                      <img src="${vehicleImage}" alt="" style="width: 100%; height: 500px" />
                    </tr>
                  </tbody>
                </table>
              </div>
              <!-- container -->
            </body>
            
            </html>`,
              Charset: 'UTF-8',
            },
            Text: {
              Data: 'body_text',
              Charset: 'UTF-8',
            },
          },
          Subject: {
            Data: 'Purchase Request',
            Charset: 'UTF-8',
          },
        },
      },
      ConfigurationSetName: null,
      EmailTags: [],
    };
    try {
      console.log('Here', params);
    pinpointEmail.sendEmail(params, (error, data) => {
      if (error) {
        console.log(error.message);
      } else {
        console.log('Email sent! Message ID: ', data.MessageId);
      }
    });

    res.status(200).json({
      success: true,
      message: 'Email sent',
    });
  } catch (error) {
    res.status(500).json({
      success: false,
      message: error,
    });
  }
});

// ii) Handling unavailable routes
app.all('*', (req, res, next) => {
  res.status(404).json({
    status: false,
    message: `can not find ${req.originalUrl} on this server`,
  });
});

// exporting app module
module.exports = app;
